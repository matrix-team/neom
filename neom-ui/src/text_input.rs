use crate::{
    app::NeomUi,
    sizes::*,
    types::{LastError, State},
};
use eframe::egui;
use egui::color::Color32;
use std::path::Path;

impl NeomUi {
    pub fn text_input(&mut self, state: &mut State, ctx: &egui::Context) {
        egui::TopBottomPanel::bottom("input")
            .frame(egui::Frame {
                // TODO: try line height instead
                inner_margin: egui::style::Margin {
                    top: MARGIN_TEXT_INPUT_TOP,
                    bottom: 0.0,
                    left: MARGIN_TEXT_X,
                    right: MARGIN_TEXT_X,
                },
                fill: egui::style::Visuals::dark().extreme_bg_color,
                ..Default::default()
            })
            .show(ctx, |ui| {
                let input: &String = &ui
                    .memory()
                    .data
                    .get_temp(egui::Id::new("input_str"))
                    .unwrap_or_default();
                self.input_str = input.to_string();
                if self.input_str.is_empty() {
                    state.editing = None;
                }
                let resp = ui.add(
                    egui::TextEdit::multiline(&mut self.input_str)
                        .text_color(Color32::WHITE)
                        .frame(false)
                        .desired_rows(1)
                        .desired_width(f32::INFINITY)
                        .cursor_at_end(true),
                );
                if let Some(roomid) = state.current_room.as_ref() {
                    if ui.input().key_pressed(egui::Key::Enter) {
                        let msg = if let Some(img) = input.strip_prefix("/img ") {
                            let image_path = Path::new(img);
                            let content = neom_client::MessageBody::image(image_path);
                            if content.is_none() {
                                state.last_error = Some(LastError::message(format!(
                                    "File {:?} does not exist",
                                    image_path
                                )));
                            }
                            if state.replying_to.is_some() {
                                state.last_error = Some(LastError::message(
                                    "Cannot reply with an image".to_string(),
                                ));
                                None
                            } else {
                                content
                            }
                        } else if let Some(stripped) = input.strip_prefix('/') {
                            Some(neom_client::MessageBody::plaintext(stripped.to_owned()))
                        } else {
                            Some(neom_client::MessageBody::plaintext(input.to_owned()))
                        };

                        if msg.is_none() || input.is_empty() {
                            self.input_str.clear()
                        } else if !ui.input().modifiers.shift && !input.is_empty() {
                            self.send_request(neom_client::Request::SendToRoom {
                                room_id: roomid.clone(),
                                body: msg.unwrap(),
                                reply_to: state
                                    .replying_to
                                    .as_ref()
                                    .map(|e| e.original_event.clone())
                                    .into(),
                                replaces: state
                                    .editing
                                    .as_ref()
                                    .map(|e| e.original_event.clone())
                                    .into(),
                            });
                            self.input_str.clear();
                            state.scroll_to_row = Some(state.get_last_row_of_current_room());
                            state.replying_to = None;
                        }
                    } else if ui.input().key_pressed(egui::Key::PageUp) {
                        state.scroll_delta = Some(PGUPDOWN_SCROLL_AMOUNT);
                    } else if ui.input().key_pressed(egui::Key::PageDown) {
                        state.scroll_delta = Some(-PGUPDOWN_SCROLL_AMOUNT);
                    }
                }
                resp.request_focus();
                egui::warn_if_debug_build(ui);

                // Store the input string in ui.memory as self.input_str gets modified before
                // sending it, resulting in unwanted newlines when sending
                ui.memory()
                    .data
                    .insert_temp(egui::Id::new("input_str"), self.input_str.clone());
            });
    }
}
