#![forbid(unsafe_code)]
#![warn(clippy::all, rust_2018_idioms)]

use neom_client::Error;
use std::fs;

use log::{error, warn};
use simplelog::{
    ColorChoice, CombinedLogger, ConfigBuilder, LevelFilter, SharedLogger, TermLogger,
    TerminalMode, WriteLogger,
};

mod app;
mod error_bar;
mod login_dialog;
mod message_body;
mod room;
mod room_list;
mod sizes;
mod text_input;
mod top_bar;
mod types;
mod user_color;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let logger_config = ConfigBuilder::new().add_filter_allow("neom".into()).build();
    let mut loggers: Vec<Box<dyn SharedLogger>> = vec![TermLogger::new(
        LevelFilter::Trace,
        logger_config.clone(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )];
    let log_file_path = "/tmp/neom.log";
    if let Ok(file) = fs::File::create(log_file_path) {
        loggers.push(WriteLogger::new(LevelFilter::Debug, logger_config, file));
    } else {
        warn!("Could not create log file: {}", log_file_path);
    }
    let _ = CombinedLogger::init(loggers);
    let config_path = home::home_dir()
        .map(|mut home| {
            home.push(".config");
            home.push("neom");
            home.push("config.toml");
            home
        })
        .unwrap();
    let (mut channels_client, channels_ui) = neom_client::channels::create_channels();
    let req = channels_ui.request.clone();
    let login_ok = if let Ok(config_str) = fs::read_to_string(&config_path) {
        if let Ok(config) = toml::from_str(&config_str) {
            let config: neom_client::config::ClientConfig = config;
            req.send(neom_client::Request::Login {
                username: config.username.clone(),
                password: config.password.clone(),
                homeserver: config.homeserver,
            })
            .await
            .unwrap();
            true
        } else {
            false
        }
    } else {
        false
    };

    if !login_ok {
        // TODO: Create own error type
        channels_client
            .events
            .send((
                None,
                neom_client::Event::LoginError(Error::AuthenticationRequired),
            ))
            .await
            .unwrap();
    }

    tokio::spawn(async move {
        loop {
            if let Err(e) = neom_client::run(&mut channels_client).await {
                match e {
                    Error::Url(_) => {
                        channels_client
                            .events
                            .send((None, neom_client::Event::LoginError(e)))
                            .await
                            .unwrap();
                    }
                    e => error!("event pump: {:?}", e),
                }
            }
        }
    });

    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "neom",
        native_options,
        Box::new(|cc| Box::new(app::NeomUi::new(cc, channels_ui))),
    );

    Ok(())
}
