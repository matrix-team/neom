use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct ClientConfig {
    pub username: String,
    pub password: String,
    pub homeserver: String,
}
