use crate::{
    app::NeomUi,
    sizes::*,
    types::{LastError, State},
};
use chrono::{prelude::*, Duration};
use eframe::egui;
use egui::color::Color32;

impl NeomUi {
    pub fn error_bar(&mut self, state: &mut State, ctx: &egui::Context) {
        if matches!(state.last_error, Some(LastError::Message { .. })) {
            egui::TopBottomPanel::top("error_bar").show(ctx, |ui| {
                let expired = if let Some(LastError::Message { time, text }) = &state.last_error {
                    ui.centered_and_justified(|ui| {
                        ui.label(
                            egui::RichText::new(text)
                                .color(Color32::RED)
                                .size(FONT_SIZE_TOP_BAR)
                                .strong(),
                        );
                    });
                    Utc::now() - *time > Duration::seconds(5)
                } else {
                    false
                };

                if expired {
                    state.last_error = None;
                }
            });
        }
    }
}
