use chrono::prelude::*;
use egui_extras::RetainedImage;
use log::debug;
use std::{
    borrow::Cow,
    collections::{BTreeMap, HashMap, HashSet, VecDeque},
    fmt,
    sync::Arc,
};

pub struct Room {
    pub id: String,
    pub name: String,
    pub message_ids: HashSet<String>,
    pub history: VecDeque<Message>,
    pub reply_targets: Vec<Message>,
    pub replacements: HashMap<String, Message>,
    pub prev_batch: Option<String>,
    pub notifications: u64,
    pub highlight: bool,
}

#[derive(Clone, Debug)]
pub enum BodyPart {
    CodeBlock(String),
    Emphasis(String),
    InlineCode(String),
    List(Vec<Vec<BodyPart>>),
    Newline,
    Paragraph(Vec<BodyPart>),
    Quote(Vec<BodyPart>),
    Reply(Vec<BodyPart>),
    RichReply(String),
    StrikeThrough(String),
    Strong(String),
    Text(String),
    Spoiler(String, bool),
    Url { label: String, target: String },
    User { id: String, pretty: String },
}
impl BodyPart {
    pub fn text(&'_ self) -> Cow<'_, str> {
        // TODO: buggy and rough.
        // reimplement with a proper stateful thingy to convert Vec<BodyPart> into a plaintext
        // string, like we do for messages when rendering
        match self {
            BodyPart::Text(text) => text.into(),
            BodyPart::Spoiler(text, _) => format!("<spoiler>{}</spoiler>", text).into(),
            BodyPart::Emphasis(text) => format!("_{}_", text).into(),
            BodyPart::InlineCode(text) => text.into(),
            BodyPart::CodeBlock(text) => format!("```\n{}\n```", text).into(),
            BodyPart::List(parts) => {
                let mut s = parts
                    .iter()
                    .map(|p| format!("- {}\n", MessageBody::Rich(p.to_owned())))
                    .collect::<String>();
                let _ = s.pop();
                s.into()
            }
            BodyPart::Newline => "\n".into(),
            // TODO: fetch the actual message to which this
            // reply is targeting
            BodyPart::RichReply(_) => "".into(),
            BodyPart::Reply(parts) | BodyPart::Paragraph(parts) => {
                let mut s = parts
                    .iter()
                    .map(|p| format!("{}\n", p.text()))
                    .collect::<String>();
                let _ = s.pop();
                s.into()
            }
            BodyPart::StrikeThrough(text) => format!("~~{}~~", text).into(),
            BodyPart::Strong(text) => format!("**{}**", text).into(),
            BodyPart::Quote(parts) => {
                let mut s = parts
                    .iter()
                    .map(|p| format!("{}\n", p.text()))
                    .collect::<String>();
                let _ = s.pop();
                format!("> {}", s).into()
            }
            BodyPart::Url { target, .. } => format!(" {}", target).into(),
            BodyPart::User { id, pretty } => format!("{}({})", pretty, id).into(),
        }
    }
}

#[derive(Clone)]
pub enum MessageBody {
    Debug(String),
    Plain(String),
    Rich(Vec<BodyPart>),
    Image {
        url: String,
        image: Arc<RetainedImage>,
    },
}
impl MessageBody {
    pub fn mark_as_edited(&mut self) {
        match self {
            MessageBody::Plain(ref mut text) => {
                *text = format!("{} (edited)", text);
            }
            MessageBody::Rich(ref mut parts) => {
                parts.push(BodyPart::Emphasis("(edited)".into()));
            }
            b => {
                debug!("Don't know how to mark {:?} to edited", b);
            }
        }
    }
}
impl Default for MessageBody {
    fn default() -> MessageBody {
        MessageBody::Debug(String::new())
    }
}
impl fmt::Debug for MessageBody {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            MessageBody::Debug(s) => write!(f, "Debug ({})", s),
            MessageBody::Plain(s) => write!(f, "Plain ({})", s),
            MessageBody::Rich(s) => write!(f, "Rich ({:?})", s),
            MessageBody::Image { .. } => write!(f, "Image"),
        }
    }
}
impl fmt::Display for MessageBody {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            MessageBody::Debug(s) => write!(f, "{}", s),
            MessageBody::Plain(s) => write!(f, "{}", s),
            MessageBody::Rich(parts) => {
                let ret = parts.iter().fold(String::new(), |mut acc, part| {
                    acc.push_str(&format!("{}", part.text()));
                    acc
                });
                write!(f, "{}", &ret[..])
            }
            MessageBody::Image { .. } => write!(f, "Image"),
        }
    }
}

#[derive(Clone)]
pub struct Message {
    pub original_event: neom_client::AnyMessageLikeEvent,
    pub id: String,
    pub body: MessageBody,
    pub sender: String,
    pub sender_id: String,
    pub avatar_url: Option<String>,
    pub formatted_time: String,
}
impl fmt::Debug for Message {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.id)
    }
}

pub enum LastError {
    Login(String),
    Message { time: DateTime<Utc>, text: String },
}
impl LastError {
    pub fn message<T: ToString>(text: T) -> LastError {
        LastError::Message {
            time: Utc::now(),
            text: text.to_string(),
        }
    }
}

#[cfg_attr(feature = "persistence", derive(serde::Deserialize, serde::Serialize))]
#[cfg_attr(feature = "persistence", serde(default))] // if we add new fields, give them default values when deserializing old state
#[derive(Default)]
pub struct State {
    pub own_id: Option<String>,
    pub last_event_id: HashMap<String, neom_client::OwnedEventId>,
    pub last_error: Option<LastError>,
    pub avatars: BTreeMap<String, RetainedImage>,
    pub current_room: Option<String>,
    pub rooms: BTreeMap<String, Room>,
    pub room_order: Vec<(String, String)>,
    pub scroll_to_row: Option<usize>,
    pub scroll_delta: Option<f32>,
    pub replying_to: Option<Message>,
    pub editing: Option<Message>,
    pub unknown_avatars: Vec<String>,
    pub unknown_events: Vec<neom_client::Request>,
}
impl State {
    pub fn room(&self) -> Option<impl Iterator<Item = &Message>> {
        self.current_room
            .as_ref()
            .map(|roomid| self.rooms[roomid].history.iter())
    }

    pub fn current_room(&self) -> Option<&Room> {
        self.current_room.as_ref().map(|room| &self.rooms[room])
    }

    pub fn current_room_mut(&mut self) -> Option<&mut Room> {
        self.current_room
            .as_ref()
            .and_then(|room| self.rooms.get_mut(room))
    }

    pub fn get_last_row_of_current_room(&self) -> usize {
        self.current_room().unwrap().history.len().max(1) - 1
    }
}

#[derive(Default)]
pub struct LoginDialog {
    pub homeserver: String,
    pub username: String,
    pub password: String,
    pub save_to_file: bool,
}
