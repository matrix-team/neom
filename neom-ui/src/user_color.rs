use eframe::egui;
use std::cmp::min;

pub fn color_from_id(user_id: &str) -> egui::Color32 {
    let hash = format!("{:?}", md5::compute(user_id));

    let r =
        u8::from_str_radix(&hash[0..2], 16).expect("valid md5 hash contains only valid hex values");
    let g =
        u8::from_str_radix(&hash[2..4], 16).expect("valid md5 hash contains only valid hex values");
    let b =
        u8::from_str_radix(&hash[4..6], 16).expect("valid md5 hash contains only valid hex values");

    let rgb_min = min(b, min(r, g));
    let (r, g, b) = match (rgb_min == r, rgb_min == g, rgb_min == b) {
        (true, _, _) => (0x00, g, b),
        (_, true, _) => (r, 0x00, b),
        (_, _, true) => (r, g, 0x00),
        _ => unreachable!("minimum value must be found"),
    };

    egui::Color32::from_rgb(r, g, b)
}
