use crate::{app::NeomUi, sizes::*, types::State};
use eframe::egui;
use egui::Color32;
use std::borrow::Cow;

impl NeomUi {
    pub fn room_list(&mut self, state: &mut State, ctx: &egui::Context) {
        egui::SidePanel::left("side_panel")
            .frame(egui::containers::Frame::none())
            .show(ctx, |ui| {
                if state.rooms.is_empty() {
                    ui.add(egui::Spinner::new());
                } else {
                    ui.spacing_mut().item_spacing = egui::vec2(0.0, 0.0);
                    egui::ScrollArea::vertical().show(ui, |ui| {
                        for (_, room_id) in &state.room_order {
                            let room = state.rooms.get(room_id).unwrap();
                            let button_contents = if room.notifications > 0 {
                                Cow::Owned(format!("{} ({})", room.name, room.notifications))
                            } else {
                                Cow::Borrowed(&room.name)
                            };
                            let frame = egui::containers::Frame {
                                outer_margin: egui::style::Margin::same(0.0),
                                inner_margin: egui::style::Margin::same(MARGIN_TEXT_X),
                                stroke: egui::Stroke::new(0.0, Color32::TRANSPARENT),
                                fill: if state.current_room.as_ref() == Some(room_id) {
                                    Color32::DARK_GRAY
                                } else if room.highlight {
                                    Color32::DARK_RED
                                } else if room.notifications > 0 {
                                    Color32::DARK_BLUE
                                } else {
                                    let hovered = ui
                                        .memory()
                                        .data
                                        .get_temp(egui::Id::new(room_id))
                                        .unwrap_or(false);
                                    if hovered {
                                        ui.style().visuals.code_bg_color
                                    } else {
                                        ui.style().visuals.faint_bg_color
                                    }
                                },
                                ..Default::default()
                            };
                            let item = frame
                                .show(ui, |ui| {
                                    ui.set_width(ui.available_width());
                                    ui.colored_label(Color32::WHITE, button_contents.as_ref());
                                })
                                .response
                                .interact(egui::Sense::click());
                            if item.clicked() {
                                state.current_room = Some(room_id.clone());
                                state.scroll_to_row = Some(state.get_last_row_of_current_room());
                                if let Some(event_id) = state.last_event_id.remove(room_id) {
                                    self.send_request(neom_client::Request::ReadMarker {
                                        room_id: room_id.to_owned(),
                                        event_id: event_id.to_owned(),
                                    });
                                }
                            }
                            if ui.input().key_pressed(egui::Key::PageUp) {
                                ui.scroll_with_delta(egui::Vec2::new(0.0, 15.0));
                            } else if ui.input().key_pressed(egui::Key::PageDown) {
                                ui.scroll_with_delta(egui::Vec2::new(0.0, -15.0));
                            }
                            ui.memory()
                                .data
                                .insert_temp(egui::Id::new(room_id), item.hovered());
                        }
                    });
                }
            });
    }
}
