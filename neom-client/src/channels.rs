use crate::{Event, Request};
use matrix_sdk::ruma::MilliSecondsSinceUnixEpoch;
use tokio::sync::{
    mpsc,
    mpsc::{Receiver, Sender},
};

#[derive(Debug)]
pub struct ClientChannels {
    pub request: Receiver<Request>,
    pub events: Sender<(Option<MilliSecondsSinceUnixEpoch>, Event)>,
}

pub struct UiChannels {
    pub request: Sender<Request>,
    pub events: Receiver<(Option<MilliSecondsSinceUnixEpoch>, Event)>,
}

pub fn create_channels() -> (ClientChannels, UiChannels) {
    let (evt_tx, evt_rx) = mpsc::channel(100);
    let (req_tx, req_rx) = mpsc::channel(100);

    (
        ClientChannels {
            request: req_rx,
            events: evt_tx,
        },
        UiChannels {
            request: req_tx,
            events: evt_rx,
        },
    )
}
