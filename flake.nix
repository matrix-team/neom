{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
    crane = {
      url = "github:ipetkov/crane";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
        rust-overlay.follows = "rust-overlay";
      };
    };
  };

  outputs = { self, nixpkgs, flake-utils, rust-overlay, crane }:
    let
      # List of systems this flake.nix has been tested to work with
      systems = [ "x86_64-linux" ];
      pkgsForSystem = system: nixpkgs.legacyPackages.${system}.appendOverlays [
        rust-overlay.overlays.default
      ];
    in
    flake-utils.lib.eachSystem systems
      (system:
       let
         name = "neom";
         pkgs = pkgsForSystem system;
         craneLib = crane.mkLib pkgs;
         neom-libpath = pkgs.lib.makeLibraryPath [
           pkgs.fontconfig
           pkgs.freetype
           pkgs.libGL
           pkgs.libxkbcommon
           pkgs.openssl
           pkgs.wayland
           # for X11 support
           pkgs.xorg.libXcursor
           pkgs.xorg.libXrandr
           pkgs.xorg.libXi
           pkgs.xorg.libX11
         ];
         mkNeom = ({ browser ? null }:
           let
             neom-nativebuildinputs = [
               pkgs.cmake
               pkgs.lld
               pkgs.pkg-config
             ];
             neom-buildinputs = [
               pkgs.expat
               pkgs.fontconfig
               pkgs.freetype
               pkgs.openssl
             ];
             neom-deps = craneLib.buildDepsOnly {
               pname = "${name}-unwrapped";
               version = "0.1.0-dev";
               nativeBuildInputs = neom-nativebuildinputs;
               buildInputs = neom-buildinputs;
               src = craneLib.cleanCargoSource (craneLib.path ./.);
             };
             neom-unwrapped = craneLib.buildPackage {
               pname = "${name}-unwrapped";
               cargoArtifacts = neom-deps;
               src = pkgs.lib.cleanSourceWith {
                 src = craneLib.path ./.;
                 filter = path: type:
                   (craneLib.filterCargoSources path type) ||
                   (type == "directory") ||
                   (pkgs.lib.hasSuffix ".desktop" path) ||
                   (pkgs.lib.hasSuffix ".ttf" path);
               };
               version = "0.1.0-dev";
               nativeBuildInputs = neom-nativebuildinputs;
               buildInputs = neom-buildinputs;

               postInstall = ''
                 cp assets/neom.desktop $out
               '';
             };
           in
           pkgs.stdenv.mkDerivation {
             inherit name;
             src = neom-unwrapped;
             binPath = pkgs.lib.makeBinPath [
               pkgs.xdg-utils
               browser
             ];
             nativeBuildInputs = [ pkgs.makeWrapper ];
             buildCommand = ''
               mkdir -p $out/bin $out/share/applications
               cp $src/neom.desktop $out/share/applications
               cp $src/bin/neom-ui $out/bin/neom
               wrapProgram $out/bin/neom \
                 --set LD_LIBRARY_PATH ${neom-libpath} \
                 --set PATH $binPath
             '';
           });

      in rec {
        packages = {
          neom = mkNeom { };
          neom-ff = mkNeom { browser = pkgs.firefox; };
          neom-chrome = mkNeom { browser = pkgs.google-chrome; };
          neom-chromium = mkNeom { browser = pkgs.chromium; };

          default = packages.neom;
        };

        devShell = pkgs.mkShell {
          buildInputs = [
            pkgs.cmake
            pkgs.lld
            pkgs.rust-bin.stable.latest.default
            pkgs.pkg-config
            pkgs.openssl
            pkgs.fontconfig
          ];
          shellHook = ''
            export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${neom-libpath}
          '';
        };
      }
    );
}


