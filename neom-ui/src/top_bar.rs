use crate::{app::NeomUi, sizes::*, types::State};
use eframe::egui;
use egui::color::Color32;

impl NeomUi {
    pub fn top_bar(&mut self, state: &mut State, ctx: &egui::Context) {
        egui::TopBottomPanel::top("top_bar").show(ctx, |ui| {
            if let Some(ref current_room) = state.current_room {
                ui.centered_and_justified(|ui| {
                    let room_name = &state.rooms.get(current_room).unwrap().name;
                    ui.label(
                        egui::RichText::new(room_name)
                            .color(Color32::WHITE)
                            .size(FONT_SIZE_TOP_BAR)
                            .strong(),
                    );
                });
            }
        });
    }
}
