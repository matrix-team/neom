# neom

[neom](https://sr.ht/~detegr/neom/) is a client software for
[Matrix](https://matrix.org/), an open network for secure, decentralized
communication.

The goal of neom is to be a client that is light enough to be able to smoothly
run on GNU/Linux smartphones and have a feature set that supports all of the
Matrix's chat features. Video conferencing and VoIP are out of scope of this
project.

## Why?

I started the project because I could not find a Matrix client that would run
on my PinePhone and work nicely with image and video attachments. I'm
developing this for my personal use at the moment.

## Contributing

[Bug/feature tracker](https://todo.sr.ht/~detegr/neom)

[Mailing list](https://lists.sr.ht/~detegr/neom)

If you're not familiar with sourcehut, you can check out the [man page](https://man.sr.ht/).

## License

[BSD-3-Clause](https://git.sr.ht/~detegr/neom/tree/master/item/LICENSE)

## Screenshot

![UI screenshot](https://git.sr.ht/~detegr/neom/blob/2c85461967b9d69c913919ac47267111b3f4f847/neom-ui.png)
