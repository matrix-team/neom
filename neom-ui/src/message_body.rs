use crate::types::*;
use egui_extras::RetainedImage;
use html_parser::{Dom, Node};
use log::{error, warn};
use std::sync::Arc;
use url::Url;

fn replace_with_rich_reply(
    reply_parts: &mut Vec<BodyPart>,
    room: &Room,
    current_event_id: &str,
    target_event_id: &str,
) -> Option<neom_client::Request> {
    if let Some(msg) = room
        .reply_targets
        .iter()
        .chain(room.history.iter())
        .find(|m| m.id == target_event_id)
    {
        let user = BodyPart::User {
            id: msg.sender_id.clone(),
            pretty: msg.sender.clone(),
        };
        reply_parts.clear();
        reply_parts.push(BodyPart::Quote(vec![
            user,
            BodyPart::RichReply(target_event_id.into()),
        ]));
        None
    } else {
        Some(neom_client::Request::GetReply {
            for_message: current_event_id.into(),
            room_id: room.id.clone(),
            event: target_event_id.into(),
        })
    }
}

pub fn replace_fallback_or_insert_rich_reply(
    message_body: &mut MessageBody,
    room: &Room,
    current_event_id: &str,
    target_event_id: &str,
) -> Vec<neom_client::Request> {
    let mut ret = Vec::new();
    if let MessageBody::Rich(ref mut parts) = message_body {
        let mut i = 0;
        let mut mx_reply_found = false;

        // The usual case where <mx-reply> is present
        while i < parts.len() {
            if let BodyPart::Reply(ref mut reply_parts) = parts[i] {
                mx_reply_found = true;
                if let Some(req) =
                    replace_with_rich_reply(reply_parts, room, current_event_id, target_event_id)
                {
                    ret.push(req);
                }
            }
            i += 1;
        }

        if !mx_reply_found {
            let mut reply_parts = vec![];
            if let Some(req) =
                replace_with_rich_reply(&mut reply_parts, room, current_event_id, target_event_id)
            {
                ret.push(req);
            }
            if !reply_parts.is_empty() {
                parts.insert(0, reply_parts[0].clone());
            }
        }
    }
    ret
}

fn detect_urls(body: BodyPart) -> Vec<BodyPart> {
    let text = match body {
        BodyPart::Text(text) => text,
        BodyPart::Emphasis(text) => text,
        BodyPart::Strong(text) => text,
        BodyPart::StrikeThrough(text) => text,
        BodyPart::List(items) => {
            return vec![BodyPart::List(
                items
                    .into_iter()
                    .map(|parts| parts.into_iter().flat_map(detect_urls).collect())
                    .collect(),
            )];
        }
        BodyPart::Paragraph(parts) => {
            return vec![BodyPart::Paragraph(
                parts.into_iter().flat_map(detect_urls).collect(),
            )]
        }
        BodyPart::Quote(parts) => {
            return vec![BodyPart::Quote(
                parts.into_iter().flat_map(detect_urls).collect(),
            )]
        }
        body => return vec![body],
    };

    let mut urls = vec![];
    for word in text.split_whitespace() {
        if let Ok(url) = Url::parse(word) {
            if url.has_host() {
                urls.push(url.to_string());
            }
        }
    }
    if urls.is_empty() {
        vec![BodyPart::Text(text)]
    } else {
        let mut parts = vec![&text[..]];
        for url in &urls {
            parts = parts
                .into_iter()
                .flat_map(|p| {
                    if p.contains(url) {
                        p.split(&url[..])
                    } else {
                        // Try without a possible trailing slash
                        p.split(&url[..url.len() - 1])
                    }
                })
                .collect::<Vec<_>>();
        }
        urls.sort_by_key(|a| text.find(a).unwrap());
        parts
            .into_iter()
            .zip(0..)
            .flat_map(|(txt, i)| {
                let mut ret = vec![BodyPart::Text(txt.into())];
                if let Some(url) = urls.get(i) {
                    ret.push(BodyPart::Url {
                        label: url.clone(),
                        target: url.into(),
                    });
                }
                ret
            })
            .collect()
    }
}

pub fn parse(body: neom_client::MessageBody) -> Option<MessageBody> {
    match body {
        neom_client::MessageBody::Text { plain, formatted } => {
            if let Some(formatted) = formatted {
                match parse_formatted(&formatted).unwrap_or(MessageBody::Plain(plain)) {
                    MessageBody::Rich(parts) => Some(MessageBody::Rich(
                        parts.into_iter().flat_map(detect_urls).collect(),
                    )),
                    MessageBody::Plain(text) => {
                        Some(MessageBody::Rich(detect_urls(BodyPart::Text(text))))
                    }
                    _ => unreachable!(),
                }
            } else {
                Some(MessageBody::Rich(detect_urls(BodyPart::Text(plain))))
            }
        }
        neom_client::MessageBody::Image {
            url_or_title, data, ..
        } => {
            if let Ok(image) = RetainedImage::from_image_bytes("image", &data) {
                if image.size().iter().any(|size| *size > 16384) {
                    // Textures that have a side larger than 16384 are not supported by egui_glow
                    return Some(MessageBody::Debug("Image too large".into()));
                }
                Some(MessageBody::Image {
                    url: url_or_title,
                    image: Arc::new(image),
                })
            } else {
                Some(MessageBody::Rich(vec![
                    BodyPart::Text("[".into()),
                    BodyPart::Url {
                        label: "image".into(),
                        target: url_or_title,
                    },
                    BodyPart::Text("]".into()),
                ]))
            }
        }
    }
}

fn parse_formatted(formatted: &str) -> Result<MessageBody, html_parser::Error> {
    Ok(MessageBody::Rich(
        Dom::parse(formatted)?
            .children
            .iter()
            .flat_map(parse_node)
            .collect::<Vec<_>>()
            .concat(),
    ))
}

fn parse_node(node: &Node) -> Result<Vec<BodyPart>, html_parser::Error> {
    match node {
        Node::Element(e) => match &e.name[..] {
            "p" => Ok(vec![BodyPart::Paragraph(
                e.children
                    .iter()
                    .flat_map(parse_node)
                    .collect::<Vec<_>>()
                    .concat(),
            )]),
            "li" => Ok(e
                .children
                .iter()
                .flat_map(parse_node)
                .collect::<Vec<_>>()
                .concat()),
            "mx-reply" => {
                let parts = vec![BodyPart::Reply(
                    e.children
                        .iter()
                        .flat_map(parse_node)
                        .collect::<Vec<_>>()
                        .concat(),
                )];
                Ok(parts)
            }
            "blockquote" => Ok(vec![BodyPart::Quote(
                e.children
                    .iter()
                    .flat_map(parse_node)
                    .collect::<Vec<_>>()
                    .concat(),
            )]),
            "ul" => Ok(vec![BodyPart::List(
                e.children.iter().flat_map(parse_node).collect::<Vec<_>>(),
            )]),
            "ol" => Ok(vec![BodyPart::List(
                // TODO: render ordered lists differently
                e.children.iter().flat_map(parse_node).collect::<Vec<_>>(),
            )]),
            "pre" => {
                let parts = e
                    .children
                    .iter()
                    .flat_map(parse_node)
                    .collect::<Vec<_>>()
                    .concat();
                let mut text = parts
                    .iter()
                    .map(|p| format!("{}\n", p.text()))
                    .collect::<String>();
                let _ = text.pop();
                Ok(vec![BodyPart::CodeBlock(text)])
            }
            "code" => {
                if e.children.is_empty() {
                    error!("code element with no children: {:?}", e);
                    return Err(html_parser::Error::Parsing(
                        "code element with no children".into(),
                    ));
                }
                if let Node::Text(code) = &e.children[0] {
                    Ok(vec![BodyPart::InlineCode(
                        String::from(html_escape::decode_html_entities(code))
                            .trim_end()
                            .into(),
                    )])
                } else {
                    Err(html_parser::Error::Parsing(
                        "code element with non-text children".into(),
                    ))
                }
            }
            "strong" => {
                if let Node::Text(text) = &e.children[0] {
                    Ok(vec![BodyPart::Strong(text.clone())])
                } else {
                    error!("i element with no children");
                    Err(html_parser::Error::Parsing(
                        "i element with no children".into(),
                    ))
                }
            }
            "i" | "em" => {
                if let Node::Text(text) = &e.children[0] {
                    Ok(vec![BodyPart::Emphasis(text.clone())])
                } else {
                    error!("i element with no children");
                    Err(html_parser::Error::Parsing(
                        "i element with no children".into(),
                    ))
                }
            }
            "del" => {
                if let Node::Text(text) = &e.children[0] {
                    Ok(vec![BodyPart::StrikeThrough(text.clone())])
                } else {
                    error!("i element with no children");
                    Err(html_parser::Error::Parsing(
                        "i element with no children".into(),
                    ))
                }
            }
            "br" => Ok(vec![BodyPart::Newline]),
            "a" => {
                let target = e.attributes["href"].as_ref().unwrap();
                let mut label = target.clone();
                if !e.children.is_empty() && matches!(e.children[0], Node::Text(_)) {
                    if let Node::Text(text) = &e.children[0] {
                        label = text.clone()
                    }
                }
                if target.starts_with("https://matrix.to/#/") {
                    Ok(vec![BodyPart::User {
                        id: target
                            .strip_prefix("https://matrix.to/#/")
                            .expect("checked earlier that it starts with the prefix")
                            .to_string(),
                        pretty: label,
                    }])
                } else {
                    Ok(vec![BodyPart::Url {
                        label,
                        target: target.to_string(),
                    }])
                }
            }
            "span" => {
                let mut text = String::new();
                if !e.children.is_empty() && matches!(e.children[0], Node::Text(_)) {
                    if let Node::Text(txt) = &e.children[0] {
                        text = txt.clone();
                    }
                }
                Ok(if e.attributes.contains_key("data-mx-spoiler") {
                    vec![BodyPart::Spoiler(text, false)]
                } else {
                    vec![BodyPart::Text(text)]
                })
            }
            _ => {
                warn!("Unhandled element: {:?}", e);
                Ok(vec![BodyPart::Text(format!("{:?}", e))])
            }
        },
        Node::Text(text) => Ok(vec![BodyPart::Text(
            html_escape::decode_html_entities(&text).into(),
        )]),
        z => {
            println!("{:?}", z);
            Err(html_parser::Error::Parsing("NYI".into()))
        }
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn test_reply_parsing() {
        let body = neom_client::MessageBody::Text {
            plain: "> <@foo:matrix.org> hello\nthis is a reply".into(),
            formatted: Some(
                r#"<mx-reply><blockquote><a href="https://matrix.to/#/room:matrix.org/eventid?via=matrix.org?via=test">In reply to</a> <a href="https://matrix.to/#/@foo:matrix.org">@foo:matrix.org</a><br>hello</blockquote></mx-reply>this is a reply. some funny characters: &lt;&gt;&quot;&amp;&quot;"#.into(),
            ),
        };
        match super::parse(body) {
            Some(super::MessageBody::Rich(r)) => {
                assert_eq!(r.len(), 2);
                assert!(matches!(r[0], super::BodyPart::Reply { .. }));
                assert!(matches!(r[1], super::BodyPart::Text(_)));
                if let super::BodyPart::Text(text) = &r[1] {
                    assert_eq!(text, r#"this is a reply. some funny characters: <>"&""#);
                }
            }
            _ => assert!(false),
        }
    }
}
