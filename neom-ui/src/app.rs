use crate::{message_body, sizes::*, types::*};
use chrono::{offset::TimeZone, Local, NaiveDateTime};
use eframe::egui;
use egui_extras::RetainedImage;
use log::error;
use neom_client::MilliSecondsSinceUnixEpoch;
use once_cell::sync::Lazy;
use std::{
    collections::{btree_map::Entry, HashMap, HashSet, VecDeque},
    default::Default,
    sync::Mutex,
};
use tokio::sync::mpsc::{Receiver, Sender};

static STATE: Lazy<Mutex<State>> = Lazy::new(|| Mutex::new(State::default()));

pub struct NeomUi {
    pub tx: Sender<neom_client::Request>,
    pub login_dialog: LoginDialog,
    pub input_str: String,
}

impl NeomUi {
    pub fn new(
        cc: &eframe::CreationContext<'_>,
        channels: neom_client::channels::UiChannels,
    ) -> NeomUi {
        let ctx = &cc.egui_ctx;
        let evt_ctx = cc.egui_ctx.clone();
        tokio::spawn(async move {
            event_handler_task(channels.events, evt_ctx).await;
        });

        ctx.set_visuals(egui::style::Visuals::dark());

        let mut font_defs = egui::FontDefinitions::default();
        font_defs.font_data.insert(
            "dejavu".into(),
            egui::FontData::from_static(include_bytes!("../fonts/DejaVuSans.ttf")),
        );
        font_defs.font_data.insert(
            "dejavu_monospace".into(),
            egui::FontData::from_static(include_bytes!("../fonts/DejaVuBQNSansMono.ttf")),
        );
        font_defs
            .families
            .get_mut(&egui::FontFamily::Monospace)
            .unwrap()
            .insert(0, "dejavu_monospace".to_owned());
        font_defs
            .families
            .get_mut(&egui::FontFamily::Proportional)
            .unwrap()
            .insert(0, "dejavu_monospace".to_owned());
        font_defs
            .families
            .get_mut(&egui::FontFamily::Proportional)
            .unwrap()
            .insert(0, "dejavu".to_owned());
        ctx.set_fonts(font_defs);

        let mut style = (*ctx.style()).clone();
        style
            .text_styles
            .get_mut(&egui::TextStyle::Body)
            .unwrap()
            .size = FONT_SIZE_NORMAL;
        style
            .text_styles
            .get_mut(&egui::TextStyle::Button)
            .unwrap()
            .size = FONT_SIZE_LARGE;
        style
            .text_styles
            .get_mut(&egui::TextStyle::Monospace)
            .unwrap()
            .size = FONT_SIZE_NORMAL;

        //style.debug.debug_on_hover = true;
        //style.debug.show_expand_width = true;
        //style.debug.show_expand_height = true;
        ctx.set_style(style);

        NeomUi {
            tx: channels.request,
            input_str: String::new(),
            login_dialog: LoginDialog {
                ..Default::default()
            },
        }
    }

    pub fn send_request(&self, req: neom_client::Request) -> bool {
        self.tx.try_send(req).is_ok()
    }
}

async fn event_handler_task(
    mut rx: Receiver<(Option<MilliSecondsSinceUnixEpoch>, neom_client::Event)>,
    ctx: egui::Context,
) {
    while let Some(event) = rx.recv().await {
        let mut state = STATE.lock().unwrap();
        handle_client_event(&mut state, event);
        while let Ok(event) = rx.try_recv() {
            handle_client_event(&mut state, event);
        }
        ctx.request_repaint();
    }
    error!("EVENT HANDLER TASK EXITED! Event receiver dropped.");
}

impl eframe::App for NeomUi {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        let mut state = STATE.lock().unwrap();
        if matches!(state.last_error, Some(LastError::Login(_))) {
            self.login_dialog(&state, ctx);
        } else {
            self.top_bar(&mut state, ctx);
            self.error_bar(&mut state, ctx);
            self.room_list(&mut state, ctx);
            self.text_input(&mut state, ctx);
            self.room(&mut state, ctx);
        }
        let mut sent_avatar_requests = vec![];
        for avatar_url in &state.unknown_avatars {
            if self.send_request(neom_client::Request::GetAvatar {
                avatar_url: avatar_url.clone(),
            }) {
                sent_avatar_requests.push(avatar_url.clone());
            }
        }
        state
            .unknown_avatars
            .retain(|url| !sent_avatar_requests.contains(url));

        let mut keep_unknown_events = vec![];
        for evt in &state.unknown_events {
            keep_unknown_events.push(!self.send_request(evt.clone()));
        }
        let mut keep_iter = keep_unknown_events.iter();
        state.unknown_events.retain(|_| *keep_iter.next().unwrap());
    }

    fn clear_color(&self, visuals: &egui::Visuals) -> egui::Rgba {
        visuals.window_fill().into()
    }
}

fn handle_client_event(
    state: &mut State,
    event: (Option<MilliSecondsSinceUnixEpoch>, neom_client::Event),
) {
    let formatted_time = if let (Some(time), _) = event {
        let naivedt = NaiveDateTime::from_timestamp_opt(time.as_secs().into(), 0)
            .expect("could not format time");
        let localdt = TimeZone::from_utc_datetime(&Local, &naivedt);
        localdt.format("%H:%M").to_string()
    } else {
        String::new()
    };
    match event.1 {
        neom_client::Event::LoginOk(user_id) => {
            // TODO: Login probably needs its own state, not last_error
            state.last_error = None;
            state.own_id = Some(user_id);
        }
        neom_client::Event::LoginError(e) => {
            state.last_error = Some(LastError::Login(e.to_string()));
        }
        neom_client::Event::Room {
            room_id,
            room_name,
            prev_batch,
            notifications,
        } => {
            if state.current_room.is_none() {
                state.current_room = Some(room_id.clone());
            }
            match state.rooms.entry(room_id.clone()) {
                Entry::Vacant(e) => {
                    if state.room_order.iter().find(|e| e.0 == room_name).is_none() {
                        let i = state.room_order.partition_point(|e| e.0 < room_name);
                        state.room_order.insert(i, (room_name.clone(), room_id.clone()));
                    }
                    e.insert(Room {
                        id: room_id,
                        name: room_name,
                        message_ids: HashSet::new(),
                        history: VecDeque::new(),
                        reply_targets: Vec::new(),
                        replacements: HashMap::new(),
                        prev_batch,
                        notifications: notifications.notification_count,
                        highlight: notifications.highlight_count > 0,
                    });
                }
                Entry::Occupied(mut e) => {
                    let room = e.get_mut();
                    room.notifications = notifications.notification_count;
                    room.highlight = notifications.highlight_count > 0;
                }
            }
        }
        neom_client::Event::Message {
            avatar_url,
            body,
            in_reply_to,
            original_event,
            position,
            ref event_id,
            room_id,
            sender,
            sender_id,
        } => {
            if let Some(ref url) = avatar_url {
                if !state.avatars.contains_key(url) {
                    state.unknown_avatars.push(url.clone());
                }
            }
            let id = event_id.to_string();
            let room = &mut state.rooms.get_mut(&room_id).unwrap();
            let is_reply_target = matches!(position, neom_client::MessagePosition::Reply(_));
            if !is_reply_target {
                if room.message_ids.contains(&id) {
                    return;
                }
                room.message_ids.insert(id.clone());
            }
            let mut targeted_by = None;
            let mut replace_tgt = None;
            let push_fn: Option<Box<dyn Fn(&mut VecDeque<Message>, Message)>> = match position {
                neom_client::MessagePosition::Front(batch_end) => {
                    if state.scroll_to_row.is_none() {
                        state.scroll_to_row = Some(0);
                    } else {
                        state.scroll_to_row = state.scroll_to_row.map(|x| x + 1);
                    }
                    room.prev_batch = Some(batch_end);
                    Some(Box::new(VecDeque::push_front))
                }
                neom_client::MessagePosition::Back => {
                    state
                        .last_event_id
                        .insert(room_id.clone(), event_id.clone());
                    Some(Box::new(VecDeque::push_back))
                }
                neom_client::MessagePosition::Reply(for_message) => {
                    targeted_by = Some(for_message);
                    None
                }
                neom_client::MessagePosition::Replace(tgt) => {
                    replace_tgt = Some(tgt);
                    None
                }
            };
            let mut message_body = message_body::parse(body);
            if message_body.is_none() {
                return;
            }

            if let Some(target) = in_reply_to {
                for req in message_body::replace_fallback_or_insert_rich_reply(
                    message_body.as_mut().unwrap(),
                    room,
                    &id,
                    &target,
                ) {
                    state.unknown_events.push(req);
                }
            }

            let mut msg = Message {
                original_event,
                id: id.clone(),
                sender,
                sender_id,
                avatar_url,
                body: message_body.unwrap(),
                formatted_time,
            };
            match push_fn {
                Some(push_fn) => push_fn(&mut room.history, msg),
                None => {
                    // If some message targets this message as a reply, change the body of the
                    // message to contain this message, fetching the possible missing messages on
                    // the way.
                    if let Some(targeted_by) = targeted_by {
                        room.reply_targets.push(msg);
                        let new_body = {
                            if let Some(tgt) = room
                                .history
                                .iter_mut()
                                .chain(room.reply_targets.iter_mut())
                                .find(|m| m.id == targeted_by)
                            {
                                let mut message_body = tgt.body.clone();
                                for req in message_body::replace_fallback_or_insert_rich_reply(
                                    &mut message_body,
                                    room,
                                    &targeted_by,
                                    &id,
                                ) {
                                    state.unknown_events.push(req);
                                }
                                Some(message_body)
                            } else {
                                None
                            }
                        };
                        if let Some(body) = new_body {
                            let tgt = room
                                .history
                                .iter_mut()
                                .chain(room.reply_targets.iter_mut())
                                .find(|m| m.id == targeted_by)
                                .unwrap();
                            tgt.body = body;
                        }
                    } else if let Some(replace_tgt) = replace_tgt {
                        msg.body.mark_as_edited();
                        room.replacements.insert(replace_tgt, msg);
                    }
                }
            }
        }
        neom_client::Event::Debug {
            original_event,
            room_id,
            sender_id,
            sender,
            body,
            position,
        } => {
            let push = if position == neom_client::MessagePosition::Back {
                VecDeque::push_back
            } else {
                VecDeque::push_front
            };
            let id = original_event.event_id().into();
            push(
                &mut state.rooms.get_mut(&room_id).unwrap().history,
                Message {
                    id,
                    original_event,
                    sender,
                    sender_id,
                    avatar_url: None,
                    body: MessageBody::Debug(body),
                    formatted_time,
                },
            );
        }
        neom_client::Event::AvatarData {
            avatar_url,
            avatar_data,
            ..
        } => {
            if !state.avatars.contains_key(&avatar_url) && !avatar_data.is_empty() {
                if let Ok(avatar) = RetainedImage::from_image_bytes("avatar", &avatar_data) {
                    state.avatars.insert(avatar_url, avatar);
                } else {
                    error!("Failed to load avatar {}", avatar_url);
                }
            }
        }
    }
}
