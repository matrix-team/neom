use crate::user_color::color_from_id;
use crate::{
    app::NeomUi,
    sizes::*,
    types::{BodyPart, MessageBody, Room, State},
};
use eframe::egui;
use egui::color::Color32;
use log::error;

const MORE_ROWS_THRESHOLD: f32 = 6.0;

impl NeomUi {
    pub fn room(&mut self, state: &mut State, ctx: &egui::Context) {
        if state.room().is_none() {
            return;
        }
        egui::CentralPanel::default().show(ctx, |ui| {
            egui::ScrollArea::vertical()
                .stick_to_bottom(true)
                .show(ui, |ui| {
                    ui.with_layout(egui::Layout::bottom_up(egui::Align::LEFT), |ui| {
                        let mut show_spoilers = vec![];
                        egui::Grid::new("chat").num_columns(2).show(ui, |ui| {
                            let needs_more_rows =
                                *ui.min_rect().y_range().end() > MORE_ROWS_THRESHOLD;

                            if let Some(delta) = state.scroll_delta.take() {
                                ui.scroll_with_delta(egui::Vec2::new(0.0, delta));
                            }
                            let mut scroll_to_row = state.scroll_to_row;
                            if needs_more_rows {
                                if let Some(room) = state.current_room_mut() {
                                    if let Some(prev_batch) = room.prev_batch.take() {
                                        self.send_request(neom_client::Request::MoreRows {
                                            room_id: state.current_room.as_ref().unwrap().clone(),
                                            prev_batch,
                                        });
                                        scroll_to_row = Some(0);
                                    }
                                }
                            }

                            let room = state.current_room();
                            if room.is_none() {
                                return;
                            }
                            let room = room.unwrap();
                            let mut prev_sender = None;
                            let mut prev_time = None;
                            let mut replying_to = None;
                            let mut editing = None;
                            let mut reply_or_edit = false;
                            for (i, msg) in room.history.iter().enumerate() {
                                let orig_msg = msg;
                                let msg = room.replacements.get(&msg.id).unwrap_or(msg);
                                let popup_id = ui.make_persistent_id(&msg.id);
                                if Some(&msg.sender) != prev_sender {
                                    let user_color = color_from_id(&msg.sender_id);
                                    if msg.avatar_url.is_some()
                                        && state
                                            .avatars
                                            .contains_key(msg.avatar_url.as_ref().unwrap())
                                    {
                                        ui.allocate_ui(
                                            egui::Vec2::new(AVATAR_SIZE, AVATAR_SIZE),
                                            |ui| {
                                                state.avatars[msg.avatar_url.as_ref().unwrap()]
                                                    .show_size(
                                                        ui,
                                                        egui::Vec2::new(AVATAR_SIZE, AVATAR_SIZE),
                                                    );
                                            },
                                        );
                                    } else {
                                        ui.allocate_ui(
                                            egui::Vec2::new(AVATAR_SIZE, AVATAR_SIZE),
                                            |ui| {
                                                let rect = ui.available_rect_before_wrap();
                                                ui.painter().circle_filled(
                                                    rect.center(),
                                                    AVATAR_SIZE / 2.,
                                                    user_color,
                                                );
                                                ui.centered_and_justified(|ui| {
                                                    ui.colored_label(
                                                        Color32::WHITE,
                                                        msg.sender
                                                            .split(' ')
                                                            .take(2)
                                                            .map(|s| {
                                                                if let Some(c) = s.chars().next() {
                                                                    if c.is_ascii_alphabetic() {
                                                                        c.to_ascii_uppercase()
                                                                    } else {
                                                                        ' '
                                                                    }
                                                                } else {
                                                                    ' '
                                                                }
                                                            })
                                                            .collect::<String>(),
                                                    );
                                                });
                                            },
                                        );
                                    }
                                    prev_sender = Some(&msg.sender);
                                    ui.label(
                                        egui::RichText::new(&msg.sender)
                                            .color(user_color)
                                            .size(FONT_SIZE_LARGE)
                                            .strong(),
                                    );
                                    ui.end_row();
                                }
                                if Some(&msg.formatted_time) != prev_time {
                                    ui.vertical(|ui| {
                                        ui.style_mut().wrap = Some(false);
                                        ui.set_max_width(AVATAR_SIZE);
                                        ui.label(&msg.formatted_time);
                                    });
                                    prev_time = Some(&msg.formatted_time);
                                } else {
                                    // Need to draw something here to make the grid UI work
                                    // correctly
                                    ui.allocate_space(egui::vec2(0.0, 0.0));
                                }
                                let mut msg_resp = ui.vertical(|ui| {
                                    ui.set_width(ui.available_rect_before_wrap().width());
                                    if state.replying_to.is_some() {
                                        ui.set_enabled(
                                            state.replying_to.as_ref().unwrap().id == msg.id[..],
                                        );
                                    } else if state.editing.is_some() {
                                        ui.set_enabled(
                                            state.editing.as_ref().unwrap().id == msg.id[..],
                                        );
                                    } else {
                                        ui.set_enabled(true);
                                    }
                                    ui.horizontal_wrapped(|ui| {
                                        render_message_body(ui, &msg.body, room)
                                    })
                                });
                                ui.end_row();

                                msg_resp.response.id = popup_id;
                                let resp = msg_resp.response.interact(egui::Sense::click());

                                if let Some(row) = scroll_to_row {
                                    if row == i {
                                        msg_resp.response.scroll_to_me(Some(egui::Align::TOP));
                                        if needs_more_rows {
                                            scroll_to_row = state.scroll_to_row;
                                        } else {
                                            scroll_to_row = None;
                                        }
                                    }
                                }

                                if resp.clicked() || resp.secondary_clicked() {
                                    ui.memory().toggle_popup(popup_id);
                                }

                                egui::popup::popup_below_widget(ui, popup_id, &resp, |ui| {
                                    let has_spoilers = match msg.body {
                                        MessageBody::Rich(ref parts) => parts
                                            .iter()
                                            .any(|p| matches!(p, BodyPart::Spoiler(_, false))),
                                        _ => false,
                                    };
                                    if has_spoilers && ui.button("Show spoilers").clicked() {
                                        show_spoilers.push(orig_msg.id.clone());
                                    }
                                    let edit_txt =
                                        if state.editing.as_ref().map(|r| &r.id) == Some(&msg.id) {
                                            "Stop editing"
                                        } else {
                                            "Edit"
                                        };
                                    if &msg.sender_id == state.own_id.as_ref().unwrap()
                                        && ui.button(edit_txt).clicked()
                                    {
                                        reply_or_edit = true;
                                        if state.editing.as_ref().map(|r| &r.id) != Some(&msg.id) {
                                            editing = Some(msg.clone());
                                            ui.memory().data.insert_temp(
                                                egui::Id::new("input_str"),
                                                format!("{}", msg.body),
                                            );
                                            replying_to = None;
                                        } else {
                                            ui.memory().data.insert_temp(
                                                egui::Id::new("input_str"),
                                                String::new(),
                                            );
                                        }
                                    }
                                    let reply_txt = if state.replying_to.as_ref().map(|r| &r.id)
                                        == Some(&msg.id)
                                    {
                                        "Do not reply"
                                    } else {
                                        "Reply"
                                    };
                                    if ui.button(reply_txt).clicked() {
                                        reply_or_edit = true;
                                        if state.replying_to.as_ref().map(|r| &r.id)
                                            != Some(&msg.id)
                                        {
                                            replying_to = Some(msg.clone());
                                            if state.editing.is_some() {
                                                ui.memory().data.insert_temp(
                                                    egui::Id::new("input_str"),
                                                    String::new(),
                                                );
                                            }
                                            editing = None;
                                        }
                                    }
                                    if ui.button("Copy text to clipboard").clicked() {
                                        ctx.output().copied_text = msg.body.to_string();
                                    }
                                    if let MessageBody::Image { url, .. } = &msg.body {
                                        if ui.button("Open in browser").clicked() {
                                            if let Err(e) = std::process::Command::new("xdg-open")
                                                .arg(url)
                                                .spawn()
                                            {
                                                error!("could not spawn a web browser: {}", e);
                                            }
                                        }
                                    }
                                });
                            }
                            if reply_or_edit {
                                state.replying_to = replying_to;
                                state.editing = editing;
                            }
                            state.scroll_to_row = scroll_to_row;
                        });
                        for spoiler_msg_id in show_spoilers {
                            let room = state.current_room_mut().unwrap();
                            let msg =
                                room.replacements
                                    .get_mut(&spoiler_msg_id)
                                    .unwrap_or_else(|| {
                                        room.history
                                            .iter_mut()
                                            .find(|m| m.id == spoiler_msg_id)
                                            .unwrap()
                                    });
                            if let MessageBody::Rich(ref mut parts) = msg.body {
                                for part in parts.iter_mut() {
                                    if let BodyPart::Spoiler(_, visible) = part {
                                        *visible = true;
                                    };
                                }
                            }
                        }
                    });
                });
        });
    }
}

fn render_message_body(ui: &mut egui::Ui, body: &MessageBody, room: &Room) {
    match body {
        MessageBody::Plain(text) => {
            ui.colored_label(Color32::WHITE, text);
        }
        MessageBody::Rich(parts) => {
            render_body_parts(ui, parts, 0, room);
        }
        MessageBody::Image { image, .. } => {
            let mut max_size = ui.available_size();
            max_size.y = IMAGE_SIZE_MAX_Y;
            image.show_max_size(ui, max_size);
        }
        MessageBody::Debug(text) => {
            egui::CollapsingHeader::new(
                text.chars()
                    .take(30)
                    .take_while(|c| *c != '(')
                    .collect::<String>(),
            )
            .id_source(text)
            .show(ui, |ui| ui.label(text));
        }
    }
}

fn render_body_parts<'a>(
    ui: &mut egui::Ui,
    parts: impl IntoIterator<Item = &'a BodyPart>,
    depth: usize,
    room: &Room,
) {
    let mut render_newlines = false;
    let parts_iter = parts.into_iter();
    let last = parts_iter.size_hint().1.unwrap();
    for (i, part) in parts_iter.enumerate() {
        render_body_part(
            ui,
            part,
            &mut render_newlines,
            i,
            i == (last - 1),
            depth,
            room,
        );
    }
}

fn render_newline(ui: &mut egui::Ui) {
    let orig_spacing = ui.spacing().item_spacing;
    ui.spacing_mut().item_spacing = egui::vec2(0.0, 0.0);
    ui.label("\n");
    ui.spacing_mut().item_spacing = orig_spacing;
}

fn render_body_part(
    ui: &mut egui::Ui,
    part: &BodyPart,
    render_newlines: &mut bool,
    idx: usize,
    last: bool,
    depth: usize,
    room: &Room,
) {
    if *render_newlines {
        render_newline(ui);
        render_newline(ui);
    }
    match part {
        BodyPart::Text(text) => {
            ui.colored_label(Color32::WHITE, text);
            *render_newlines = false;
        }
        BodyPart::Spoiler(text, visible) => {
            if *visible {
                ui.colored_label(Color32::GREEN, text);
            } else {
                ui.colored_label(Color32::GREEN, "[spoiler]");
            }
            *render_newlines = false;
        }
        BodyPart::Newline => {
            if !last {
                render_newline(ui);
            }
            *render_newlines = false;
        }
        BodyPart::Paragraph(parts) => {
            ui.horizontal_wrapped(|ui| {
                render_body_parts(ui, parts, depth, room);
            });
            *render_newlines = true;
        }
        BodyPart::User { id, pretty } => {
            ui.label(
                egui::RichText::new(pretty)
                    .color(color_from_id(id))
                    .strong(),
            )
            .on_hover_text(format!("https://matrix.to/#/{}", id));
        }
        BodyPart::CodeBlock(code) => {
            if idx != 0 {
                render_newline(ui);
            }
            ui.group(|ui| {
                ui.set_width(ui.available_rect_before_wrap().width());
                ui.label(egui::RichText::new(code).color(Color32::WHITE).monospace())
            });
            *render_newlines = true;
        }
        BodyPart::InlineCode(code) => {
            ui.label(egui::RichText::new(code).color(Color32::WHITE).code());
            *render_newlines = false;
        }
        BodyPart::Reply(parts) => {
            render_body_parts(ui, parts, depth + 1, room);

            if !last && depth == 0 {
                render_newline(ui);
                render_newline(ui);
            }
            *render_newlines = false;
        }
        BodyPart::RichReply(to_message) => {
            if let Some(msg) = room
                .history
                .iter()
                .chain(room.reply_targets.iter())
                .find(|m| m.id == *to_message)
            {
                let body = room
                    .replacements
                    .get(&msg.id)
                    .map(|e| &e.body)
                    .unwrap_or_else(|| &msg.body);
                ui.horizontal_wrapped(|ui| {
                    render_message_body(ui, body, room);
                });
            } else {
                ui.label(format!("Unknown rich reply message id {}", to_message));
            }
        }
        BodyPart::Quote(parts) => {
            ui.vertical(|ui| {
                ui.indent((), |ui| {
                    render_body_parts(ui, parts, depth + 1, room);
                });
            });

            // Some clients send quotes without a paragraph after the quote
            if !last && depth == 0 {
                render_newline(ui);
                render_newline(ui);
            }
            *render_newlines = false;
        }
        BodyPart::Url { label, target } => {
            ui.hyperlink_to(label, target);
            *render_newlines = false;
        }
        BodyPart::Emphasis(text) => {
            ui.label(egui::RichText::new(text).color(Color32::WHITE).italics());
            *render_newlines = false;
        }
        BodyPart::Strong(text) => {
            ui.label(egui::RichText::new(text).color(Color32::WHITE).strong());
            *render_newlines = false;
        }
        BodyPart::StrikeThrough(text) => {
            ui.label(
                egui::RichText::new(text)
                    .color(Color32::WHITE)
                    .strikethrough(),
            );
            *render_newlines = false;
        }
        BodyPart::List(elems) => {
            let last = elems.iter().size_hint().1.unwrap();
            for (i, elem) in elems.iter().enumerate() {
                ui.label("• ");
                render_body_parts(ui, elem, depth, room);
                if i != (last - 1) {
                    render_newline(ui);
                }
            }
            *render_newlines = true;
        }
    }
}
