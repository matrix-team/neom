use log::{debug, error, info};
use matrix_sdk::{config::SyncSettings, reqwest::Url, room::Room, Client, LoopCtrl, Result};
use mime::{self, Mime};
use pulldown_cmark::{html, Options, Parser};
use std::{
    borrow::{Borrow, Cow},
    convert::TryFrom,
    fs,
    path::Path,
    str::FromStr,
};
use tokio::sync::mpsc::Sender;

pub mod channels;
use channels::ClientChannels;

pub mod config;

use matrix_sdk::{
    attachment::AttachmentConfig,
    deserialized_responses::{SyncResponse, UnreadNotificationsCount},
    media::{MediaFormat, MediaRequest, MediaThumbnailSize},
    room::MessagesOptions,
    ruma::{
        api::client::media::get_content_thumbnail::v3::Method,
        events::{
            self,
            room::{
                message::{InReplyTo, Relation, Replacement, RoomMessageEventContent},
                MediaSource,
            },
            AnyMessageLikeEventContent, AnySyncMessageLikeEvent, AnySyncTimelineEvent,
            AnyTimelineEvent, MessageLikeEvent, OriginalMessageLikeEvent, SyncMessageLikeEvent,
        },
        exports::serde_json::{self, value::RawValue},
        serde::{JsonObject, Raw},
        OwnedMxcUri, OwnedRoomId, OwnedUserId,
    },
};

pub use matrix_sdk::{
    ruma::{
        events::AnyMessageLikeEvent, EventId, MilliSecondsSinceUnixEpoch, OwnedEventId, RoomId,
        UserId,
    },
    Error,
};

#[derive(Clone, Debug)]
pub enum MessageBody {
    Text {
        plain: String,
        formatted: Option<String>,
    },
    Image {
        url_or_title: String,
        mime: Option<Mime>,
        data: Vec<u8>,
    },
}
impl MessageBody {
    pub fn plaintext(text: String) -> MessageBody {
        MessageBody::Text {
            plain: text,
            formatted: None,
        }
    }
    pub fn image(path: &Path) -> Option<MessageBody> {
        if let Ok(true) = path.try_exists() {
            let data = fs::read(path).ok()?;
            let filename = path.file_name().unwrap().to_string_lossy().to_string();
            let parts = filename.split('.').collect::<Vec<_>>();
            let mime = match *parts.last()? {
                "bmp" => mime::IMAGE_BMP,
                "gif" => mime::IMAGE_GIF,
                "jpeg" => mime::IMAGE_JPEG,
                "jpg" => mime::IMAGE_JPEG,
                "png" => mime::IMAGE_PNG,
                "svg" => mime::IMAGE_SVG,
                _ => return None,
            };
            Some(MessageBody::Image {
                url_or_title: filename,
                mime: Some(mime),
                data,
            })
        } else {
            None
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum MessagePosition {
    Front(String),
    Back,
    Reply(String),
    Replace(String),
}

#[derive(Debug, Clone)]
pub enum Request {
    Login {
        homeserver: String,
        username: String,
        password: String,
    },
    SendToRoom {
        room_id: String,
        body: MessageBody,
        reply_to: Box<Option<events::AnyMessageLikeEvent>>,
        replaces: Box<Option<events::AnyMessageLikeEvent>>,
    },
    MoreRows {
        room_id: String,
        prev_batch: String,
    },
    ReadMarker {
        room_id: String,
        event_id: OwnedEventId,
    },
    GetAvatar {
        avatar_url: String,
    },
    GetReply {
        for_message: String,
        room_id: String,
        event: String,
    },
}

#[derive(Debug)]
pub enum Event {
    Room {
        room_id: String,
        room_name: String,
        prev_batch: Option<String>,
        notifications: UnreadNotificationsCount,
    },
    Message {
        avatar_url: Option<String>,
        body: MessageBody,
        event_id: OwnedEventId,
        in_reply_to: Option<String>,
        original_event: events::AnyMessageLikeEvent,
        position: MessagePosition,
        room_id: String,
        sender: String,
        sender_id: String,
    },
    Debug {
        original_event: events::AnyMessageLikeEvent,
        room_id: String,
        sender_id: String,
        sender: String,
        body: String,
        position: MessagePosition,
    },
    AvatarData {
        avatar_url: String,
        avatar_data: Vec<u8>,
    },
    LoginError(Error),
    LoginOk(String),
}

async fn avatar_data_from_url(
    client: &Client,
    url: Option<&OwnedMxcUri>,
) -> matrix_sdk::Result<Vec<u8>> {
    // FIXME: Do not cache as we cache the results in neom-ui anyway?
    match url {
        Some(url) => {
            client
                .media()
                .get_media_content(
                    &MediaRequest {
                        source: MediaSource::Plain(url.clone()),
                        format: MediaFormat::Thumbnail(MediaThumbnailSize {
                            method: Method::Scale,
                            // TODO: Share the avatar size between the UI and the client
                            width: 40u32.into(),
                            height: 40u32.into(),
                        }),
                    },
                    true,
                )
                .await
        }
        None => Ok(Vec::new()),
    }
}

async fn anysynctimelineevent_to_neom_event(
    evt: &AnySyncTimelineEvent,
    client: &Client,
    room: &Room,
    room_id: &RoomId,
) -> Option<Event> {
    match evt {
        AnySyncTimelineEvent::MessageLike(AnySyncMessageLikeEvent::RoomMessage(
            SyncMessageLikeEvent::Original(e),
        )) => {
            let (member, sender, avatar_url) = get_member_data(&e.sender, room).await?;
            let replacement = get_replacement(e.content.relates_to.as_ref());
            let content = if replacement.is_some() {
                &replacement.unwrap().new_content
            } else {
                &e.content
            };
            let in_reply_to = get_reply(content.relates_to.as_ref());
            msgtype_to_neom_event(
                AnyMessageLikeEvent::RoomMessage(MessageLikeEvent::Original(
                    e.clone().into_full_event(room_id.to_owned()),
                )),
                evt.event_id(),
                client,
                room_id,
                sender,
                member,
                avatar_url,
                &content.msgtype,
                in_reply_to,
                match replacement {
                    Some(tgt) => MessagePosition::Replace(tgt.event_id.to_string()),
                    None => MessagePosition::Back,
                },
            )
            .await
            .ok()
        }
        AnySyncTimelineEvent::MessageLike(AnySyncMessageLikeEvent::Sticker(
            SyncMessageLikeEvent::Original(e),
        )) => {
            let original_event = e.clone().into_full_event(room_id.to_owned());
            let member = room
                .get_member(&e.sender)
                .await
                .expect("should be able to get room members during a sync")
                .expect("sync events should have valid members");
            let sender = member
                .display_name()
                .unwrap_or(e.sender.as_str())
                .to_owned();
            let avatar_url = member.avatar_url().map(|url| url.to_string());
            Some(Event::Message {
                original_event: events::AnyMessageLikeEvent::Sticker(MessageLikeEvent::Original(
                    original_event,
                )),
                event_id: evt.event_id().to_owned(),
                room_id: room_id.to_string(),
                sender,
                sender_id: member.user_id().to_string(),
                body: MessageBody::Text {
                    plain: format!("Sticker: {}", e.content.body.to_owned()),
                    formatted: None,
                },
                position: MessagePosition::Back,
                avatar_url,
                in_reply_to: None,
            })
        }
        e => {
            debug!("UNHANDLED EVENT:\n{:?}", e);
            None
        }
    }
}

async fn get_member_data(id: &UserId, room: &Room) -> Option<(String, String, Option<String>)> {
    match room
        .get_member(id)
        .await
        .expect("should be able to get room members during a sync")
    {
        Some(m) => {
            let sender = m.display_name().unwrap_or(id.as_str()).to_owned();
            let avatar_url = m.avatar_url().map(|url| url.to_string());

            Some((m.user_id().to_string(), sender, avatar_url))
        }
        None => {
            error!("Member with id {:?} not found in members list known to the client. Using fallbacks.", id);
            Some((id.as_str().into(), format!("{} [???]", id), None))
        }
    }
}

fn get_reply(relation: Option<&Relation>) -> Option<&EventId> {
    if let Some(Relation::Reply {
        in_reply_to: InReplyTo { event_id, .. },
    }) = relation
    {
        Some(event_id.borrow())
    } else {
        None
    }
}

fn get_replacement(relation: Option<&Relation>) -> Option<&Replacement> {
    if let Some(Relation::Replacement(r)) = relation {
        Some(r)
    } else {
        None
    }
}

async fn anyroomevent_to_neom_event(
    evt: &AnyTimelineEvent,
    position: MessagePosition,
    client: &Client,
    room: &Room,
    room_id: &RoomId,
) -> Option<Event> {
    match evt {
        AnyTimelineEvent::MessageLike(AnyMessageLikeEvent::RoomMessage(
            MessageLikeEvent::Original(e),
        )) => {
            let (member, sender, avatar_url) = get_member_data(&e.sender, room).await?;
            let replacement = get_replacement(e.content.relates_to.as_ref());
            let content = if replacement.is_some() {
                &replacement.unwrap().new_content
            } else {
                &e.content
            };
            let in_reply_to = get_reply(content.relates_to.as_ref());
            match msgtype_to_neom_event(
                AnyMessageLikeEvent::RoomMessage(MessageLikeEvent::Original(e.clone())),
                evt.event_id(),
                client,
                room_id,
                sender,
                member,
                avatar_url,
                &content.msgtype,
                in_reply_to,
                match replacement {
                    Some(tgt) => MessagePosition::Replace(tgt.event_id.to_string()),
                    None => position,
                },
            )
            .await
            {
                Ok(evt) => Some(evt),
                Err(e) => {
                    error!("Failed to convert {:?} to neom event: {}", evt, e);
                    None
                }
            }
        }
        _ => {
            debug!("UNHANDLED ROOM EVENT:\n{:?}", evt);
            None
        }
    }
}

async fn msgtype_to_neom_event(
    event: AnyMessageLikeEvent,
    event_id: &EventId,
    client: &Client,
    room_id: &RoomId,
    sender: String,
    sender_id: String,
    avatar_url: Option<String>,
    msgtype: &events::room::message::MessageType,
    in_reply_to: Option<&EventId>,
    position: MessagePosition,
) -> matrix_sdk::Result<Event> {
    match msgtype {
        events::room::message::MessageType::Text(text) => Ok(Event::Message {
            original_event: event,
            event_id: event_id.to_owned(),
            room_id: room_id.to_string(),
            sender,
            sender_id,
            body: MessageBody::Text {
                plain: text.body.to_owned(),
                formatted: text.formatted.to_owned().map(|fmt| fmt.body),
            },
            position,
            avatar_url,
            in_reply_to: in_reply_to.map(|s| s.to_string()),
        }),
        events::room::message::MessageType::Image(image) => {
            let data = client
                .media()
                .get_media_content(
                    &MediaRequest {
                        source: image.source.clone(),
                        format: MediaFormat::File,
                    },
                    true,
                )
                .await?;
            let url = match &image.source {
                MediaSource::Plain(url) => url,
                MediaSource::Encrypted(enc) => &enc.url,
            };
            Ok(Event::Message {
                original_event: event,
                event_id: event_id.to_owned(),
                room_id: room_id.to_string(),
                sender,
                sender_id,
                body: MessageBody::Image {
                    mime: None,
                    url_or_title: format!(
                        "https://{}/_matrix/media/v3/download/{}/{}",
                        url.server_name().unwrap(),
                        url.server_name().unwrap(),
                        url.media_id().unwrap()
                    ),
                    data,
                },
                position,
                avatar_url,
                in_reply_to: in_reply_to.map(|s| s.to_string()),
            })
        }
        events::room::message::MessageType::Video(video) => {
            let url = match &video.source {
                MediaSource::Plain(url) => url,
                MediaSource::Encrypted(enc) => &enc.url,
            };
            let full_url = format!(
                "https://{}/_matrix/media/v3/download/{}/{}",
                url.server_name().unwrap(),
                url.server_name().unwrap(),
                url.media_id().unwrap()
            );
            Ok(Event::Message {
                original_event: event,
                event_id: event_id.to_owned(),
                room_id: room_id.to_string(),
                sender,
                sender_id,
                body: MessageBody::Text {
                    plain: url.to_string(),
                    formatted: Some(format!("[ <a href=\"{}\">{}</a> ]", full_url, video.body)),
                },
                position,
                avatar_url,
                in_reply_to: in_reply_to.map(|s| s.to_string()),
            })
        }
        _ => {
            debug!("UNHANDLED MESSAGETYPE: {:?}", msgtype);
            Ok(Event::Debug {
                original_event: event,
                room_id: room_id.to_string(),
                sender,
                sender_id,
                body: format!("{:?}", msgtype),
                position,
            })
        }
    }
}

fn patch_event<E>(evt: &'_ Raw<E>) -> Cow<'_, Raw<E>> {
    // Messages with unsigned:null seem to be coming from a conduit bot. Either it
    // does not use standardized messages, and should be fixed, or then unsigned:null
    // parsing should be fixed in Ruma
    let mut event = Cow::Borrowed(evt);
    let mut json = event.json();
    if json.get().contains(r#""unsigned":null"#) {
        let json_str = json.get().to_owned();
        let patched = json_str.replace(r#""unsigned":null"#, r#""unsigned":{}"#);
        event = Cow::Owned(Raw::from_json(
            RawValue::from_string(patched).expect("patch unsigned:null"),
        ));
        json = event.json();
    }

    // mautrix-signal sends messages with both blurhash and xyz.amorgan.blurhash set. Ruma will
    // fail to deserialize these, so patch blurhash out for now, as it is not in the spec yet.
    if json.get().contains(r#""blurhash":""#) && json.get().contains(r#""xyz.amorgan.blurhash":""#)
    {
        if let Ok(Some(content)) = event.get_field::<JsonObject>("content") {
            if let Some(info) = content.get("info") {
                let bh = info.get("blurhash");
                let bham = info.get("xyz.amorgan.blurhash");
                if bh.is_some() && bham.is_some() {
                    let string_to_replace = format!(r#""blurhash":{},"#, bh.unwrap());
                    let json_str = json.get().to_owned();
                    let mut patched = json_str.replacen(&string_to_replace, "", 1);
                    if patched == json_str {
                        // If the replace fails, try without the trailing comma
                        patched = json_str.replacen(
                            &string_to_replace[..string_to_replace.len() - 1],
                            "",
                            1,
                        );
                    }
                    event = Cow::Owned(Raw::from_json(
                        RawValue::from_string(patched).expect("duplicate blurhash removed"),
                    ));
                }
            }
        }
    }
    event
}

async fn matrix_syncresp_to_neom_events(
    client: &Client,
    e: SyncResponse,
) -> Vec<(Option<MilliSecondsSinceUnixEpoch>, Event)> {
    let mut ret = Vec::new();
    for (room_id, room_evt) in &e.rooms.join {
        let room = client
            .get_room(room_id)
            .expect("sync events should have valid room ids");
        let room_display_name = room
            .display_name()
            .await
            .expect("sync event expected to have a valid display name for a room");
        ret.push((
            None,
            Event::Room {
                room_id: room_id.to_string(),
                room_name: format!("{}", room_display_name),
                prev_batch: room_evt.timeline.prev_batch.clone(),
                notifications: room_evt.unread_notifications,
            },
        ));
        for evt in &room_evt.timeline.events {
            let event = patch_event(&evt.event);
            match event.deserialize() {
                Ok(ref evt) => {
                    match anysynctimelineevent_to_neom_event(evt, client, &room, room_id).await {
                        Some(e) => ret.push((Some(evt.origin_server_ts().to_owned()), e)),
                        None => {
                            error!("Failed to convert sync event {:?}", evt);
                        }
                    }
                }
                Err(e) => {
                    error!("Could not deserialize sync event {:?} {}", evt, e);
                }
            }
        }
    }
    ret
}

fn anymessageevent_to_messageevent(
    ame: AnyMessageLikeEvent,
) -> Option<OriginalMessageLikeEvent<RoomMessageEventContent>> {
    match ame {
        AnyMessageLikeEvent::RoomMessage(MessageLikeEvent::Original(e)) => Some(e),
        AnyMessageLikeEvent::Sticker(MessageLikeEvent::Original(e)) => {
            Some(OriginalMessageLikeEvent {
                event_id: e.event_id.clone(),
                origin_server_ts: e.origin_server_ts,
                room_id: e.room_id.clone(),
                sender: e.sender.clone(),
                unsigned: e.unsigned.clone(),
                content: RoomMessageEventContent::text_plain(e.content.body),
            })
        }
        _ => None,
    }
}

fn markdown_to_html(plain: &str) -> String {
    let mut options = Options::empty();
    options.insert(Options::ENABLE_STRIKETHROUGH);
    let parser = Parser::new_ext(plain, options);
    let mut html_output = String::new();
    html::push_html(&mut html_output, parser);
    remove_useless_paragraphs(&html_output).to_string()
}

fn remove_useless_paragraphs(html: &str) -> &str {
    if html.starts_with("<p>") && html.ends_with("</p>\n") {
        let txt = html.strip_prefix("<p>").unwrap();
        let txt = txt.strip_suffix("</p>\n").unwrap();
        if !txt.contains("<p>") {
            return txt;
        }
    }
    html
}

fn get_content(
    plain: &str,
    reply_to: &Option<AnyMessageLikeEvent>,
) -> Option<RoomMessageEventContent> {
    if let Some(msg) = reply_to {
        anymessageevent_to_messageevent(msg.clone())
            .as_ref()
            .map(|original_event| {
                let html = markdown_to_html(plain);
                RoomMessageEventContent::text_html(plain, &html).make_reply_to(original_event)
            })
    } else {
        let html_output = markdown_to_html(plain);
        Some(RoomMessageEventContent::text_html(plain, html_output))
    }
}

async fn handle_request(
    client: &Client,
    req: Request,
    tx: Sender<(Option<MilliSecondsSinceUnixEpoch>, Event)>,
) -> serde_json::Result<()> {
    match req {
        Request::Login { .. } => {}
        Request::SendToRoom {
            room_id,
            body,
            reply_to,
            replaces,
        } => {
            let room_id = OwnedRoomId::from_str(&room_id).expect("expected a valid room id");
            let room = client
                .get_joined_room(&room_id)
                .expect("expected a valid room");
            match body {
                MessageBody::Text { plain, .. } => {
                    let (content, new_content, rel_id) = if let Some(ref r) = *replaces {
                        let mut edited_plain = plain.clone();
                        let rel = match r.original_content() {
                            Some(AnyMessageLikeEventContent::RoomMessage(
                                RoomMessageEventContent { relates_to, .. },
                            )) => relates_to,
                            _ => None,
                        };
                        edited_plain.insert_str(0, "edit: ");
                        (
                            get_content(&edited_plain, reply_to.as_ref()),
                            get_content(&plain, reply_to.as_ref()),
                            // Edits to a message must target the original event's id
                            rel.and_then(|r| {
                                get_replacement(Some(&r)).map(|r| r.event_id.to_owned())
                            }),
                        )
                    } else {
                        (get_content(&plain, reply_to.as_ref()), None, None)
                    };
                    if let Some(mut content) = content {
                        if let Some(r) = *replaces {
                            let id = match rel_id {
                                Some(id) => id,
                                None => r.event_id().to_owned(),
                            };
                            content.relates_to = Some(Relation::Replacement(Replacement::new(
                                id,
                                Box::new(new_content.unwrap()),
                            )));
                        }
                        room.send(content, None)
                            .await
                            .expect("error handling for failed messages not yet implemented");
                    }
                }
                MessageBody::Image {
                    url_or_title,
                    mime,
                    data,
                } => {
                    if let Err(e) = room.send_attachment(
                        &url_or_title,
                        mime.as_ref().unwrap(),
                        &data,
                        AttachmentConfig::new(),
                    ).await {
                        error!("Failed to send: {}", e);
                    }
                }
            }
        }
        Request::MoreRows {
            room_id,
            prev_batch,
        } => {
            let room_id = OwnedRoomId::from_str(&room_id).expect("expected a valid room id");
            let room = client.get_room(&room_id).expect("expected a valid room");
            let req = MessagesOptions::backward().from(Some(&prev_batch[..]));
            match room.messages(req).await {
                Ok(messages) => {
                    let batch_end = messages.end.unwrap_or_else(|| "".into()); // TODO
                    for evt in messages.chunk {
                        let event = patch_event(&evt.event);
                        match event.deserialize() {
                            Ok(ref evt) => {
                                match anyroomevent_to_neom_event(
                                    evt,
                                    MessagePosition::Front(batch_end.clone()),
                                    client,
                                    &room,
                                    &room_id,
                                )
                                .await
                                {
                                    Some(e) => {
                                        tx.send((Some(evt.origin_server_ts().to_owned()), e))
                                            .await
                                            .unwrap();
                                    }
                                    None => {
                                        error!("Failed to convert MoreRows event {:?}", evt);
                                    }
                                }
                            }
                            Err(e) => {
                                error!("Could not deserialize room event {:?} {}", evt, e);
                            }
                        }
                    }
                }
                Err(e) => error!("handle_request: {:?}", e),
            }
        }
        Request::ReadMarker { room_id, event_id } => {
            let id = OwnedRoomId::from_str(&room_id).expect("expected a valid room id");
            let room = client.get_joined_room(&id).expect("expected a valid room");

            if let Err(e) = room.read_marker(&event_id, Some(&event_id)).await {
                error!("Could not set room read marker: {:?}", e);
            } else {
                info!("Room read marker set to {:?}", event_id);
            }
            tx.send((
                None,
                Event::Room {
                    room_id,
                    room_name: String::new(),
                    notifications: UnreadNotificationsCount {
                        ..Default::default()
                    },
                    prev_batch: None,
                    // unused when updating a room
                },
            ))
            .await
            .unwrap();
        }
        Request::GetAvatar { avatar_url } => {
            let uri = OwnedMxcUri::from(&avatar_url[..]);
            let avatar = avatar_data_from_url(client, Some(&uri)).await;
            tx.send((
                None,
                Event::AvatarData {
                    avatar_url,
                    avatar_data: avatar.unwrap_or_default(),
                },
            ))
            .await
            .unwrap();
        }
        Request::GetReply {
            for_message,
            room_id,
            event,
        } => {
            let id = OwnedRoomId::from_str(&room_id).expect("expected a valid room id");
            let room_id = OwnedRoomId::from_str(&room_id).expect("expected a valid room id");
            let room = client.get_room(&id).expect("expected a valid room");
            let event_id = <&EventId>::try_from(&event[..]).expect("expected a valid event id");
            match room.event(event_id).await {
                Ok(evt) => {
                    let evt = patch_event(&evt.event);
                    let evt = evt.deserialize()?;
                    match anyroomevent_to_neom_event(
                        &evt,
                        MessagePosition::Reply(for_message),
                        client,
                        &room,
                        &room_id,
                    )
                    .await
                    {
                        Some(e) => {
                            tx.send((Some(evt.origin_server_ts().to_owned()), e))
                                .await
                                .unwrap();
                        }
                        None => error!("could not convert event {:?}", evt),
                    }
                }
                Err(e) => error!("could not get event: {}", e),
            }
        }
    }
    Ok(())
}

async fn login(
    homeserver: String,
    username: String,
    password: String,
    tx: Sender<(Option<MilliSecondsSinceUnixEpoch>, Event)>,
) -> Result<Client> {
    let homeserver_url = Url::from_str(&homeserver)?;
    let user = OwnedUserId::try_from(format!(
        "@{}:{}",
        username,
        homeserver_url
            .host_str()
            .expect("expected a https url. https urls always have host str")
    ))?;
    let client = Client::new(homeserver_url).await?;
    let ret = client.clone();
    tokio::spawn(async move {
        info!("Logging in...");
        if let Err(e) = client
            .login_username(user.localpart(), &password)
            .device_id("neom")
            .send()
            .await
        {
            tx.send((None, Event::LoginError(e))).await.unwrap();
            return;
        }

        tx.send((None, Event::LoginOk(user.to_string())))
            .await
            .unwrap();

        info!("Running sync loop");
        let c = &client;
        let tx = &tx;
        client
            .sync_with_callback(SyncSettings::new(), |ev| async move {
                for evt in matrix_syncresp_to_neom_events(c, ev).await {
                    tx.send(evt).await.unwrap();
                }
                LoopCtrl::Continue
            })
            .await
            .unwrap();
    });

    Ok(ret)
}

pub async fn run(channels: &mut ClientChannels) -> Result<()> {
    let rx = &mut channels.request;
    let mut client = None;
    info!("Running event pump");
    loop {
        match rx.recv().await {
            Some(Request::Login {
                homeserver,
                username,
                password,
            }) => {
                client = Some(login(homeserver, username, password, channels.events.clone()).await?)
            }
            Some(req) => {
                if let Err(e) =
                    handle_request(client.as_ref().unwrap(), req, channels.events.clone()).await
                {
                    error!("Error handling request: {}", e);
                }
            }
            None => {
                break;
            }
        }
    }
    Ok(())
}
