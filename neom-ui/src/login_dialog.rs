use crate::{
    app::NeomUi,
    sizes::*,
    types::{LastError, State},
};
use eframe::egui;
use egui::color::Color32;

impl NeomUi {
    pub fn login_dialog(&mut self, state: &State, ctx: &egui::Context) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.vertical_centered(|ui| {
                let neom = egui::RichText::new("neom")
                    .color(Color32::WHITE)
                    .size(LOGIN_HEADER_SIZE);
                ui.label(neom);
                ui.add_space(LOGIN_SPACING_1);

                ui.group(|ui| {
                    let make_label = |text| {
                        egui::RichText::new(text)
                            .color(Color32::WHITE)
                            .size(FONT_SIZE_LARGE)
                    };

                    ui.label(make_label("Homeserver"));
                    ui.text_edit_singleline(&mut self.login_dialog.homeserver);

                    ui.label(make_label("Username"));
                    ui.text_edit_singleline(&mut self.login_dialog.username);

                    ui.label(make_label("Password"));
                    ui.add(
                        egui::TextEdit::singleline(&mut self.login_dialog.password).password(true),
                    );

                    ui.add_space(LOGIN_SPACING_2);

                    let remember_me_warning_text =
                        "Saves username and password into a plaintext file";

                    ui.checkbox(&mut self.login_dialog.save_to_file, "Remember me")
                        .on_hover_text_at_pointer(remember_me_warning_text);

                    if self.login_dialog.save_to_file {
                        ui.label(remember_me_warning_text);
                    }

                    ui.add_space(LOGIN_SPACING_3);

                    if ui.button("Login").clicked() {
                        self.send_request(neom_client::Request::Login {
                            homeserver: self.login_dialog.homeserver.clone(),
                            username: self.login_dialog.username.clone(),
                            password: self.login_dialog.password.clone(),
                        });
                    }

                    if let Some(LastError::Login(e)) = &state.last_error {
                        ui.colored_label(Color32::RED, format!("Login failed: {:?}", e));
                    }
                });
            });
        });
    }
}
